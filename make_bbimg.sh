#!/bin/bash

bbroot=${WORKSPACE}/build/tmp/work/armv7-none-linux-gnueabi/busybox-static-1.18.5-r43.0
devlist=${bbroot}/geninit

mkbootpath="${WORKSPACE}/build/tmp/sysroots/x86_64-linux/bin"
gencpio="${WORKSPACE}/build/tmp/work/mdm9x15-none-linux-gnueabi/linux-2.6.38-r6/kernel/usr/gen_init_cpio"

set -x

cd $bbroot
$gencpio $devlist | gzip -n -9 > initrd.gz 
#find ${bbroot}/bb-initrd | cpio -o -H newc | gzip > ${bbroot}/bb-initrd.img.gz
cd -

output=${WORKSPACE}/boot-oe-msm9615.img

kernelsize=`awk --non-decimal-data '/ _end/ {end="0x" $1} /_stext/ {beg="0x" $1} END {size1=end-beg+4096; size=and(size1,compl(4095)); printf("%#x",size)}' ${WORKSPACE}/build/tmp/work/mdm9x15-none-linux-gnueabi/linux-2.6.38-r6/kernel/System.map`

${mkbootpath}/mkbootimg --kernel ${WORKSPACE}/build/tmp/work/mdm9x15-none-linux-gnueabi/linux-2.6.38-r6/kernel/arch/arm/boot/Image \
		--ramdisk ${bbroot}/initrd.gz \
		--cmdline "root=/dev/ram rw rootwait console=ttyHSL0,115200,n8 no_console_suspend=1 androidboot.hardware=qcom" \
		--base 0x40800000 \
		--ramdisk_offset $kernelsize \
		--output $output

set +x

if [ -f $output ]; then
  echo
  echo "Image complete: $output"
  echo
fi
