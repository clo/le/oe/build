require linux.inc

PR = "r6"

# Mark archs/machines that this kernel supports
DEFAULT_PREFERENCE = "-1"
DEFAULT_PREFERENCE_mdm9x15 = "1"

SRC_URI = "file://${WORKSPACE}/kernel"

S = ${WORKDIR}/kernel

do_configure_prepend() {
        mv ${WORKDIR}/kernel/arch/arm/configs/msm9615_defconfig ${WORKDIR}/defconfig
}
