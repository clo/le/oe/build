DESCRIPTION = "Andriod Debug Bridge. It provides connectivity over USB for running a shell, transferring files etc."
HOMEPAGE = "http://developer.android.com/guide/developing/tools/adb.html"
LICENSE = "Apache-2.0"

SRC_URI = "file://${WORKSPACE}/system/core"

inherit autotools

PR = "r1"

S = "${WORKDIR}/core"

